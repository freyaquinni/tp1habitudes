package com.dinfogarneau.tp.habitudes;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivitySettings extends AppCompatActivity implements View.OnClickListener {

    public static final String USER = "USER";
    public static final String RESET = "RESET";
    private final int REQUEST_CODE = 1234;
    private Button btnSetName;
    private Button btnReset;
    private String user;
    private SharedPreferences prefs;

    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        btnSetName = (Button) findViewById( R.id.btnSetName);
        btnReset = (Button) findViewById( R.id.btnReset );

        btnSetName.setOnClickListener(this);
        btnReset.setOnClickListener(this);

        //Récuperer le nom de l'utilisateur
        Intent intent =getIntent();

        if(intent.getStringExtra("USER")!=null)
            user= intent.getStringExtra("USER");

        else{
            prefs = getSharedPreferences("MonFichierDeSauvegarde", MODE_PRIVATE);
            user= prefs.getString("user",user);
        }

        btnSetName.setText("Votre nom " + user);
    }

    @Override
    public void onClick(View v) {
        if ( v == btnSetName ) {
            //Changer le nom de l'utilisateur
            intent =new Intent(this, ActivityChangeName.class);
            intent.putExtra(USER, user);

            setResult(RESULT_OK, intent);
            startActivityForResult(intent, REQUEST_CODE);
        }else if(v== btnReset){
            //Rénitialiser les compteurs
            intent = new Intent();
            // passage de valeur
            intent.putExtra(RESET, true);
            setResult(RESULT_OK,intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode ==ActivityChangeName.RESULT_OK) {
                //Récuperer le nom de l'utilisateur
                user = data.getStringExtra(ActivityChangeName.USER);
                btnSetName.setText("Votre nom " + user);

                //Envoyer le nouveau nom à l'activité Habitude
                intent = new Intent();
                // passage de valeur
                intent.putExtra(USER, user);
                setResult(RESULT_OK,intent);
                finish();
            }
        }
    }

}