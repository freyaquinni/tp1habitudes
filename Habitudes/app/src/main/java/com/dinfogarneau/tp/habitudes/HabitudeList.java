package com.dinfogarneau.tp.habitudes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class HabitudeList {
    private List<Habitude> mHabitudeList;

    public HabitudeList(List<Habitude> mPersonList) {
        this.mHabitudeList = mHabitudeList;
    }

    public HabitudeList() {
        String[] HabitudeNames = {"Faire du jogging", "Nourrir le chat", "Jouer à league of legends"};
        this.mHabitudeList = new ArrayList<>();

        for (int i = 0; i < HabitudeNames.length; i++) {
            Habitude p = new Habitude(HabitudeNames[i]);
            mHabitudeList.add(p);
        }
    }

    public List<Habitude> getmHabitudeList() {
        return mHabitudeList;
    }

    public void setmHabitudeList(List<Habitude> mHabitudeList) {
        this.mHabitudeList = mHabitudeList;
    }

    public int setSize()
    {
        return mHabitudeList.size();
    }
}
