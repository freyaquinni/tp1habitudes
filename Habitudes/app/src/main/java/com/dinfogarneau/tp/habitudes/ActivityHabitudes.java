package com.dinfogarneau.tp.habitudes;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Guideline;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ActivityHabitudes extends AppCompatActivity implements View.OnClickListener {
    private final int REQUEST_CODE = 1234;
    public static final String USER = "USER";
    public static final String HABITUDELIST = "HABITUDELIST";
    static final String COMPTEUR = "COMPTEUR";

    private TextView tvAccueil;
    private TextView tvHabitude;
    private TextView tvScore;
    private Button btnPlus;
    private Button btnNext;
    private Button btnPrevious;
    private Button btnShare;
    private Button btnSettings;
    private Guideline guideline;

    private String nomHabitude;
    private String nom;
    private int score;
    private int compteur;

    private SharedPreferences prefs;
    //private List<Habitude> listeHabitudes;

    HabitudeList habitudeList;

    @Override
    public void onClick(View v) {
        if ( v == btnPlus ) {
            score++;
            habitudeList.getmHabitudeList().get(compteur).setScore(score);

            tvScore.setText(String.valueOf(score));
        } else if ( v == btnNext ) {
            if(compteur+1 >habitudeList.setSize()-1)
                compteur=0;
            else
                compteur++;

            nomHabitude = String.valueOf(habitudeList.getmHabitudeList().get(compteur).getNom());
            score = habitudeList.getmHabitudeList().get(compteur).getScore();

            tvHabitude.setText(nomHabitude);
            tvScore.setText(String.valueOf(score));
        } else if ( v == btnPrevious ) {
            if(compteur-1 <0){
                compteur=habitudeList.setSize()-1;
            }else
                compteur--;
            nomHabitude = String.valueOf(habitudeList.getmHabitudeList().get(compteur).getNom());
            score = habitudeList.getmHabitudeList().get(compteur).getScore();

            tvHabitude.setText(nomHabitude);
            tvScore.setText(String.valueOf(score));
        } else if ( v == btnShare ) {
            // Partager l'habitude et son score
            String habitude = String.valueOf(tvHabitude.getText() + " : " + tvScore.getText());

            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, habitude);
            intent.setType("text/plain");

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }else if ( v == btnSettings ) {
            Intent intent =new Intent(this, ActivitySettings.class);
            intent.putExtra(USER, nom);

            setResult(RESULT_OK, intent);
            startActivityForResult(intent, REQUEST_CODE);
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_habitudes);

        tvAccueil = (TextView)findViewById( R.id.tvAccueil );
        tvHabitude = (TextView)findViewById( R.id.tvHabitude );
        tvScore = (TextView)findViewById( R.id.tvScore );
        btnPlus = (Button)findViewById( R.id.btn_plus );
        btnNext = (Button)findViewById( R.id.btn_next );
        btnPrevious = (Button)findViewById( R.id.btn_previous );
        btnShare = (Button)findViewById( R.id.btnShare );
        btnSettings = (Button)findViewById( R.id.btnSettings );
        guideline = (Guideline)findViewById( R.id.guideline );

        btnPlus.setOnClickListener( this );
        btnNext.setOnClickListener( this );
        btnPrevious.setOnClickListener( this );
        btnShare.setOnClickListener( this );
        btnSettings.setOnClickListener( this );



        //Récuperer les activités
        habitudeList =new HabitudeList();

        //Récuperer les scores sauvegradés
        prefs = getSharedPreferences("MonFichierDeSauvegarde", MODE_PRIVATE);
        for(int i =0; i< habitudeList.getmHabitudeList().size(); i++){
            score = prefs.getInt("score"+ String.valueOf(i), 0);
            habitudeList.getmHabitudeList().get(i).setScore(score);
        }

        //Récuperer le nom de l'utilisateur
        Intent intent =getIntent();
        if(intent.getStringExtra("USER")!=null)
            nom= intent.getStringExtra("USER");
        else
            nom= prefs.getString("user",nom);

        tvAccueil.setText("Habitudes pour " + nom);

        //Récuperer les données de l'habitude affichée
        nomHabitude = String.valueOf(habitudeList.getmHabitudeList().get(compteur).getNom());
        score =habitudeList.getmHabitudeList().get(compteur).getScore();

        //Affichage des données
        tvHabitude.setText(nomHabitude);
        tvScore.setText(String.valueOf(score));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE) {
            if (resultCode ==ActivitySettings.RESULT_OK) {
                //Rénitialisation des scores
                boolean blnReset = data.getBooleanExtra(ActivitySettings.RESET, false);
                if(blnReset){
                    for(Habitude uneHabitude : habitudeList.getmHabitudeList()){
                        uneHabitude.setScore(0);
                    }
                }

                //Modification du nom
                if(data.getStringExtra(ActivitySettings.USER) !=null){
                    nom = data.getStringExtra(ActivitySettings.USER);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Récuperer les données de l'habitude affichée
        nomHabitude = String.valueOf(habitudeList.getmHabitudeList().get(compteur).getNom());
        score =habitudeList.getmHabitudeList().get(compteur).getScore();

        //Affichage des données
        tvHabitude.setText(nomHabitude);
        tvScore.setText(String.valueOf(score));
        tvAccueil.setText("Habitudes pour " + nom);
    }

    @Override
    protected void onPause() {
        super.onPause();

        //Sauvegarder les scores dans le fichier
        SharedPreferences.Editor editor = prefs.edit();
        for(int i =0; i< habitudeList.getmHabitudeList().size(); i++){
            editor.putInt("score" + String.valueOf(i), habitudeList.getmHabitudeList().get(i).getScore());
        }
        editor.putString("user", nom);
        editor.apply();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(USER,nom);
        outState.putInt(COMPTEUR, compteur);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        compteur = savedInstanceState.getInt(COMPTEUR, compteur);
        nom = savedInstanceState.getString(USER, nom);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}