package com.dinfogarneau.tp.habitudes;

public class Habitude {
    private String nom;
    private int Score;

    public Habitude(String nom) {
        this.nom = nom;
        this.Score = 0;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getScore() {
        return Score;
    }

    public void setScore(int Score) {
        this.Score = Score;
    }
}
