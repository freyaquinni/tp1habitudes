package com.dinfogarneau.tp.habitudes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String MDP ="MDP";
    public static final String USER = "USER";
    private final int REQUEST_CODE = 1234;

    private Button btnLogin;
    private EditText etPseudo;
    private EditText etMdp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLogin=findViewById(R.id.btnLogin);
        etPseudo=findViewById(R.id.etPseudo);
        etMdp=findViewById(R.id.etMdp);

        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent =new Intent(this, ActivityHabitudes.class);

        String pseudo = String.valueOf(etPseudo.getText());
        String mdp = String.valueOf(etMdp.getText());

        if(pseudo.equals("bot") && mdp.equals("1234")){
            intent.putExtra(USER, pseudo);
            //intent.putExtra(MDP, mdp);

            setResult(RESULT_OK, intent);
            startActivityForResult(intent, REQUEST_CODE);
        }else{
            Toast.makeText(this, "Le nom d'utilisateur ou le mot de passe saisis sont incorrectes ",
                    Toast.LENGTH_SHORT).show();
        }
    }
}