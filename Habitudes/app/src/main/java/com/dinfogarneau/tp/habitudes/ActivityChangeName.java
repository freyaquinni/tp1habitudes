package com.dinfogarneau.tp.habitudes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityChangeName extends AppCompatActivity implements View.OnClickListener {

    private Button btnOk;
    private EditText etNewName;
    private String user;
    public static final String USER = "USER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_name);

        etNewName = (EditText) findViewById( R.id.etNewName );
        btnOk = (Button) findViewById( R.id.btnOk );

        btnOk.setOnClickListener(this);


        //Récuperer le nom de l'utilisateur
        Intent intent =getIntent();
        user= intent.getStringExtra("USER");
            etNewName.setText(user);
    }

    @Override
    public void onClick(View v) {
        //Envoyer le nouveau nom
        Intent intent = new Intent();
        // passage de valeur
        user = String.valueOf(etNewName.getText());
        intent.putExtra(USER, user);
        setResult(RESULT_OK,intent);
        finish();
    }
}